# Books Website

A simple responsive website per an assignment for the discipline  "Web Technologies".
https://vfu-books.dmarinovdev.com/

## What's been used in this project

* **HTML5**
* **CSS3** (SCSS for easier and faster writing of the CSS styling)
* **JavaScript**
* **GIT** (for version control purposes)
* **Yarn** for package management 
  (For easier and faster development process a package called **browser-sync** is used. Its role is to refresh the browser each time when the CSS or HTML files are been updated.)
* **Images** - free for use images from website - https://unsplash.com/
